__attribute__ ((visibility ("default"))) int a() { return 0; }
__attribute__ ((visibility ("protected"))) int b() { return 1; }
__attribute__ ((visibility ("hidden"))) int c() { return 2; }
__attribute__ ((visibility ("internal"))) int d() { return 3; }
